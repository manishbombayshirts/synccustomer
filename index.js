const fs = require('fs');
const csv = require('csv-parser');
var request = require('request');

var dubai_store_url = 'https://e5f02761d13f3f117ba8cd3a31da37ab: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@bombayshirts-dubai-prod.myshopify.com/admin/api/2021-07/customers.json';
var usa_store_url = 'https://35ff0977a74c9610752cdf565c1ad22e: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@bombayshirts-usa-prod.myshopify.com/admin/api/2021-07/customers.json';
var india_store_url = 'https://6a5febe857e7dbdae622370cdc8f768b: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@bombayshirts-india-prod.myshopify.com/admin/api/2021-07/customers.json';

var options = {
  'method': 'POST',
  'url': india_store_url,
  'headers': {
    'Content-Type': 'application/json'
  }
};

var sleep = 0;
var file = 'output_upload_customer-in-' + new Date().getTime() + '.csv'
fs.writeFile(file, "id,email,shopify_id,error", err => {
  if (err) {
    console.error(err)
    return
  }
});

fs.createReadStream('input.csv')
  .pipe(csv({ separator: ';' }))
  .on('data', function (row) {
      setTimeout(function(){
        createUser(row);
      }, sleep);
      sleep = sleep + 500;
  });

  function createUser(row) {
    console.log("Row", row);
    var address = row['s_address'] ? row['s_address'] : "";
    if(row['s_address2']) {
      if(address) {
        address = address + ', ' + row['s_address2'];
      } else {
        address = row['s_address2'];
      }
    }
    var body = JSON.stringify({
      "customer": {
        "first_name": row['first_name'],
        "last_name": row['last_name'],
        "email": row['email'],
        "phone": row['phone_no'],
        "verified_email": true,
        "addresses": [{
          "address1": address,
          "city": row['s_city'] ? row['s_city'] : "",
          "province": row['s_state'] ? (row['s_state'] === 'TG' ? 'TS' : row['s_state']) : "",
          "phone": row['s_phone'] ? row['s_phone'] : "",
          "zip": row['s_zipcode'] ? row['s_zipcode'] : "",
          "last_name": row['s_lastname'] ? row['s_lastname'] : "",
          "first_name": row['s_firstname'] ? row['s_firstname'] : "",
          "country": row['s_country']
        }],
        "metafields": [
          {
            "key": "Meta_customer_id",
            "value": row['id'],
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_store_id",
            "value": row['store_id'] ? row['store_id'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_store_name",
            "value": row['store_id'] ? row['store_id'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_status",
            "value": row['status'] ? row['status'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_dob",
            "value": row['dob'] ? row['dob'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_notification_preference",
            "value": row['notification_preference'] ? row['notification_preference'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_family_friend",
            "value": row['family_friend'] ? row['family_friend'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_family_friend_of",
            "value": row['family_friend_of'] ? row['family_friend_of'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_gst_in",
            "value": row['gst_in'] ? row['gst_in'] : "",
            "value_type": "string",
            "namespace": "global"
          },
          {
            "key": "Meta_notes_n_comments",
            "value": row['notes_n_comments'] ? row['notes_n_comments'] : "",
            "value_type": "string",
            "namespace": "global"
          }
        ],
        "send_email_invite": false
      }
    });
    console.log("Body", body);
    options.body = body;
    request(options, function (error, response) {
      if (error) throw new Error(error);
      response = JSON.parse(response.body);
      fs.appendFile(file, "\n" + row['id'] + "," + row['email'] + "," + (response['customer'] ? response['customer']['id'] : "") + "," + (response['errors'] ? JSON.stringify(response['errors']) : ""), err => {
        if (err) {
          console.error(err)
          return
        }
      });
    });
  };

